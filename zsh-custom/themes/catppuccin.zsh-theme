# Oh-My-Zsh : Nico Theme (https://gist.github.com/ntarocco/3027ed75b6e8fc1fd119)
# Modified by : Carlos Cuesta

eval red='#E66767'
eval green='#70CF67'
eval yellow='#FACA64'
eval blue='#00BFFF'
eval peach='#FAB770'
eval pink='#AD6FF7'
eval flamingo='#F29D9D'
eval mauve='#FF8F40'
eval teal='#4CD4BD'
eval sapphire='#4BA8FA'
eval lavender='#00BBCC'
eval sky='#61BDFF'
eval white='#BDBDBD'
eval grey='#191926'

if [[ $USER == "root" ]]; then
  CARETCOLOR="$red"
else
  CARETCOLOR="$white"
fi

setopt prompt_subst
PROMPT='$(_user_host)$(_current_dir)$(git_prompt_info)
%F{$CARETCOLOR%}>%{$reset_color%} '

#PROMPT2='%F{$grey%}◀%{$reset_color%} '

RPROMPT=''
#RPROMPT='$(_vi_status)%F{$(echotc UP 1)%}$(git_prompt_short_sha) $(_git_time_since_commit) ${_return_status} %T% %F{$(echotc DO 1)%}'
#RPROMPT='$(_vi_status)%F{$(echotc UP 1)%}$(git_remote_status) $(git_prompt_short_sha) ${_return_status} %F{$white%}%T%F{$(echotc DO 1)%}%{$reset_color%}'

#local _current_dir="%F{$green%}%0~%{$reset_color%} "
local _return_status="%F{$red%}%(?..×)%{$reset_color%}"
local _hist_no="%F{$grey%}%h%{$reset_color%}"

function _current_dir() {
  echo "%F{$teal%}$(shrink_path -l -t -5 -g)%{$reset_color%} "
}

function _user_host() {
  if [[ -z "$NICKNAME" ]]; then
    echo "%F{$white%}[%F{$sapphire%}%n%{$reset_color%}%F{$white%}:%F{$pink%}%m%{$reset_color%}%F{$white%}] "
  else
    echo "[%F{$sapphire%}%n%{$reset_color%}%F{$white%}:%F{$pink%}$NICKNAME%{$reset_color%}%F{$white%}] "
  fi
}

#function _vi_status() {
  #if {echo $fpath | grep -q "plugins/vi-mode"}; then
    #echo "$(vi_mode_prompt_info)"
  #fi
#}


MODE_INDICATOR="%F{$yellow%}❮%{$reset_color%}%F{$yellow%}❮❮%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_PREFIX="%F{$white%}-> %F{$yellow%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"

#ZSH_THEME_GIT_PROMPT_DIRTY=" %F{$red%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %F{$white%}[%F{$red%}+%F{$white%}]%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=" %F{$white%}[%F{$green%}-%F{$white%}]%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE="%F{$red%}⬇%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE="%F{$green%}⬆%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIVERGED_REMOTE="%F{$yellow%}⬌%{$reset_color%}"

# Format for git_prompt_long_sha() and git_prompt_short_sha()
ZSH_THEME_GIT_PROMPT_SHA_BEFORE="%F{$white%}[%F{$yellow%}"
ZSH_THEME_GIT_PROMPT_SHA_AFTER="%F{$white%}]%{$reset_color%}"

# LS colors, made with http://geoff.greer.fm/lscolors/
export LSCOLORS="exfxcxdxbxegedabagacad"
#export LS_COLORS='di=34:ln=35:so=32:pi=33:ex=31:bd=34:cd=34:su=0:sg=0:tw=0:ow=0:'
export LS_COLORS='di=34:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43'
export GREP_COLOR='1;33'
