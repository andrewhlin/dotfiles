default: all

BASEDIR := $(shell pwd)


oh-my-zsh: 
ifeq ($(wildcard ~/.oh-my-zsh),)
	curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -o install.sh
	sh install.sh
	rm install.sh
endif

vundle:
ifeq ($(wildcard ~/.vim/bundle/Vundle.vim),)
	mkdir -p ~/.vim/bundle
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
endif

brew:
	brew install fzf htop mmv neovim ag source-highlight tree-sitter tmux virtualenvwrapper universal-ctags

symlink-oh-my-zsh: oh-my-zsh
	ln -sf $(BASEDIR)/zsh-custom/themes/materialshell-dark.zsh-theme ~/.oh-my-zsh/custom/themes/
	ln -sf $(BASEDIR)/zsh-custom/themes/clean.zsh-theme ~/.oh-my-zsh/custom/themes/
	ln -sf $(BASEDIR)/zsh-custom/themes/catppuccin.zsh-theme ~/.oh-my-zsh/custom/themes/

vimrc:
	ln -sf $(BASEDIR)/vimrc ~/.vimrc
	mkdir -p ~/.vim/
	ln -sf $(BASEDIR)/vim/indent ~/.vim/

tmux.conf:
	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
	ln -sf $(BASEDIR)/tmux.conf ~/.tmux.conf
	ln -sf $(BASEDIR)/tmuxname ~/.tmuxname
	ln -sf $(BASEDIR)/tmux-osx.conf ~/.tmux-osx.conf

gitconfig:
	ln -sf $(BASEDIR)/gitconfig ~/.gitconfig

vundle-plugins: vimrc vundle
	vim -c VundleUpdate -c quitall

lscolors: oh-my-zsh
	echo zstyle \':completion:*\' list-colors '$${(@s.:.)LS_COLORS}' >> ~/.zshrc
	echo "autoload -Uz compinit && compinit" >> ~/.zshrc

zshrc-finishers: oh-my-zsh fast-syntax
	echo 'export HOMEBREW_NO_ANALYTICS=1' >> ~/.zshrc
	echo 'export HOMEBREW_NO_GITHUB_API=1' >> ~/.zshrc
	echo 'export EDITOR=nvim' >> ~/.zshrc
	echo bindkey \'^S\' fzf-history-widget >> ~/.zshrc
	echo bindkey \'^U\' backward-kill-line >> ~/.zshrc
	echo 'export FZF_DEFAULT_OPTS=\'--height=1 --layout=reverse --color hl:\#42a5f5,fg:\#9e9e9e,fg+:\#ffffff,bg+:\#373b41,hl+:\#2196f3,pointer:\#f44336,prompt:\#8bc34a --bind=ctrl-s:page-down\'' >> ~/.zshrc
	sed -E -i.bak '/.*export PATH=.*/export PATH=$HOME\/.local\/bin:$PATH/g' ~/.zshrc
	omz plugin enable shrink-path fzf fast-syntax-highlighting

fast-syntax: oh-my-zsh
	git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git \
  ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/fast-syntax-highlighting

fast-theme: zshrc-finishers fast-syntax
	mkdir -p ~/.config/fsh/
	ln -sf $(BASEDIR)/fsh-custom/catppucin.ini ~/.config/fsh
	fast-theme XDG:catppuccin

ctags:
	mkdir -p ~/.config/
	mkdir -p ~/.local/bin
	ln -sf ${BASEDIR}/ctags ~/.config/ctags
	ln -sf /opt/homebrew/bin/ctags ~/.local/bin

neovim:
	mkdir -p ~/.config/nvim
	ln -sf ${BASEDIR}/nvim/* ~/.config/nvim/
	ln -sf $(BASEDIR)/vim/indent ~/.config/nvim/

zsh: tmux lscolors zsh-theme zshrc-finishers

clean: 
	rm -rf ~/.oh-my-zsh ~/.zshrc ~/.vim ~/.vimrc ~/.tmux.conf ~/.gitconfig ~/.viminfo

.PHONY: oh-my-zsh vundle submodules symlink-vim symlink-oh-my-zsh vimrc tmux.conf gitconfig vundle-plugins zsh-theme zshrc-finishers fast-theme neovim
.PHONY: tmux lscolors zsh ctags
.PHONY: clean
.PHONY: all
all: tmux.conf gitconfig zsh ctags neovim

