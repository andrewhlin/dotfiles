set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" plugin from http://vim-scripts.org/vim/scripts.html
Plugin 'itchyny/lightline.vim'
"Plugin 'itchyny/vim-cursorword'
Plugin 'itchyny/vim-gitbranch'
Plugin 'scrooloose/nerdcommenter'
" seems like this got moved to here, but redirects
" Plugin 'preservim/nerdcommenter'
Plugin 'ervandew/supertab'
Plugin 'OmniCppComplete'
"Plugin 'nachumk/systemverilog.vim'
Plugin 'bfrg/vim-cpp-modern'
Plugin 'keith/swift.vim'
Plugin 'Yggdroot/indentLine'
Plugin 'elzr/vim-json'
" Plugin 'vim-python/python-syntax'
"Plugin 'tpope/vim-surround'
Plugin 'gcmt/taboo.vim'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-session'
" Plugin 'preservim/tagbar'
" Git Integrations
Plugin 'rhysd/git-messenger.vim'
" Plugin 'APZelos/blamer.nvim'
" Plugin 'file:///Users/andrewhlin/Documents/blamer.nvim'
" Plugin 'xolox/vim-easytags'
" Plugin 'airblade/vim-rooter'
" File management
" This one is kind of redundant with fzf
"Plugin 'wincent/command-t'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'ludovicchabant/vim-gutentags'
Plugin 'liuchengxu/vista.vim'
" Colors
Plugin 'ssh://git@gitlab.com/andrewhlin/ayu-vim-fork.git'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

set ruler
set softtabstop=2
set expandtab
set tabstop=2
set shiftwidth=2
set hlsearch

let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified'],
      \             [ 'method', 'ctags' ] ]
      \ },
      \ 'component_function': {
      \   'method': 'NearestMethodOrFunction',
      \   'gitbranch': 'gitbranch#name',
      \   'ctags': 'gutentags#statusline',
      \ },
      \ }

syntax on
set background=dark
"colorscheme one
if has("termguicolors")
  set termguicolors
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
let ayucolor = 'dark'
colorscheme ayu
let g:lightline.colorscheme = 'ayu'
set noshowmode
set cul
set laststatus=2
set wildmenu
set wildmode=longest,full
set backspace=indent,eol,start

let g:taboo_tab_format=" %N %f%m "
let g:taboo_renamed_tab_format=" %N %l%m "
let g:session_autosave = 'no'
let g:session_autoload = 'no'
let g:CommandTFileScanner = 'find'
let g:NERDSpaceDelims = 1
" python does some stupid shit where the defualt commenter already includes
" the space nicely
let g:NERDCustomDelimiters = {'python': {'left': '#'}}
" allow fancy python highlighting
let g:python_highlight_all = 1
let g:indentLine_setColors = 0
" let g:blamer_delay = 250
" let g:blamer_prefix = ' >>> '
let g:vim_json_syntax_conceal = 0
"let g:tagbar_file_size_limit = 200000
"if executable('ag')
  "" Use ag over grep
  "set grepprg="ag --vimgrep"
  "let g:ackprg = 'ag --vimgrep'
"endif
set sessionoptions+=tabpages,globals

function! BuffersList()
  let all = range(0, bufnr('$'))
  let res = []
  for b in all
    if buflisted(b)
      call add(res, bufname(b))
    endif
  endfor
  return res
endfunction

function! GrepBuffers (expression)
  exec 'vimgrep/'.a:expression.'/ '.join(BuffersList())
endfunction

" nnoremap <leader>f
  " \ :call fzf#vim#files('.', fzf#vim#with_preview({'options': ['--query', expand('<cword>')]}))<cr>
nnoremap <leader>f :Files <CR>

function! GF(...)
  call fzf#run({'dir': a:1, 'source': 'find . -type f', 'options':['-1', '--query', expand('<cfile>')], 'sink': 'e'})
  " call fzf#vim#files('.', fzf#vim#with_preview({'options': ['--query', expand('<cword>')]}))<cr>
endfunction

function! NearestMethodOrFunction() abort
  return get(b:, 'vista_nearest_method_or_function', '??')
endfunction

let g:gutentags_define_advanced_commands=1


command! -nargs=* GF :call GF(<f-args>)
nnoremap gf :GF .<CR>
" nnoremap <leader>gb :BlamerToggle <CR>
nnoremap <leader>it :IndentLinesToggle <CR>
nnoremap <leader>sp :set invpaste <CR>
nnoremap <leader>vt :Vista!! <CR>

autocmd VimEnter * call vista#RunForNearestMethodOrFunction()
autocmd FileType python set omnifunc=python3complete#Complete

let g:SuperTabDefaultCompletionType = "context"
autocmd FileType *
  \ if &omnifunc != '' |
  \   call SuperTabChain(&omnifunc, "<c-p>") |
  \ endif

set rtp+=/opt/homebrew/opt/fzf
command! -nargs=+ Bvimgrep call GrepBuffers(<q-args>)
