require('plugins')
require('context')
require('marks-cfg')
-- require('osc52-cfg') -- deprecated by native nvim support in neovim-0.10.0
require('ayu-cfg')
require('telescope-cfg')
require('blame-cfg')
require('treesitter-cfg')
-- require('catppuccin-simple-cfg')
require('catppuccin-cfg')
require('completions')
require('lsp')
require('fold')

vim.opt.ruler = true
vim.opt.softtabstop=2
vim.opt.expandtab=true
vim.opt.tabstop=2
vim.opt.shiftwidth=2
vim.opt.hlsearch=true

vim.cmd([[syntax on]])
vim.opt.background='dark'
if vim.fn.has("termguicolors") then
  vim.opt.termguicolors = true
end
vim.cmd([[colorscheme catppuccin]])
vim.opt.cul=true
vim.opt.showmode=false
vim.opt.laststatus=2
vim.opt.wildmenu=true
vim.opt.wildmode="longest,full"
vim.opt.backspace="indent,eol,start"
vim.opt.number=true
vim.opt.relativenumber=true

-- vim.g.NERDSpaceDelims = 1
-- vim.g.NERDCustomDelimiters = {'python': {'left': '#'}}
vim.g.python_highlight_all = 1
vim.g.vim_json_syntax_conceal = 0
vim.g.git_messenger_extra_blame_args = "-w"
vim.g.gutentags_define_advanced_commands=1
vim.g.gutentags_exclude_filetypes={'c', 'cpp', 'objc', 'objcpp', 'swift'}

-- vim.api.nvim_set_hl(0, "TreesitterContextBottom", { underline = true, sp="Grey" })
-- vim.api.nvim_set_hl(0, "TreesitterContextLineNumber", { link = "Constant" })
-- vim.api.nvim_set_hl(0, "TreesitterContext", { link = "CursorLine" })
local telescope = require('telescope.builtin')
vim.keymap.set('n', '<leader>tf', telescope.find_files, {})
vim.keymap.set('n', '<leader>tg', telescope.git_files, {})
vim.keymap.set('n', '<leader>tb', telescope.buffers, {})
vim.keymap.set('n', '<leader>tl', telescope.live_grep, {})
vim.keymap.set('n', '<leader>tL', function() telescope.live_grep({grep_open_files=true}) end, {})
vim.keymap.set('n', '<leader>ts', telescope.grep_string, {})
vim.keymap.set('n', '<leader>tS', function() telescope.grep_string({grep_open_files=true}) end, {})
vim.keymap.set('n', '<leader>tq', telescope.quickfix, {})
vim.keymap.set('n', '<leader>tQ', telescope.quickfixhistory, {})
