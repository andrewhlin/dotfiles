set nocompatible              " be iMproved, required
filetype off                  " required

" let g:lightline = {
      " \ 'active': {
      " \   'left': [ [ 'mode', 'paste' ],
      " \             [ 'gitbranch', 'readonly', 'filename', 'modified'],
      " \             [ 'ctags' ] ]
      " \ },
      " \ 'component_function': {
      " \   'gitbranch': 'gitbranch#name',
      " \   'ctags': 'gutentags#statusline',
      " \ },
      " \ }

" let g:lightline.colorscheme = 'ayu2'
lua require('plugins')
lua require('context')
lua require('marks-cfg')
lua require('ayu-cfg')
lua require('telescope-cfg')
lua require('blame-cfg')
lua require('treesitter-cfg')
" lua require('catppuccin-simple-cfg')
lua require('catppuccin-cfg')
lua require('completions')
lua require('lsp')

filetype plugin indent on    " required

set ruler
set softtabstop=2
set expandtab
set tabstop=2
set shiftwidth=2
set hlsearch

" set foldmethod=expr
" set foldexpr=nvim_treesitter#foldexpr()
" set nofoldenable                     " Disable folding at startup.

syntax on
set background=dark
"colorscheme one
if has("termguicolors")
  set termguicolors
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
"let ayucolor = 'dark'
colorscheme catppuccin
set noshowmode
set cul
set laststatus=2
set wildmenu
set wildmode=longest,full
set backspace=indent,eol,start
set number
set relativenumber

hi TreesitterContextBottom gui=underline guisp=Grey
hi link TreesitterContextLineNumber Constant
hi link TreesitterContext CursorLine

" let g:taboo_tab_format=" %N %f%m "
" let g:taboo_renamed_tab_format=" %N %l%m "
" let g:CommandTFileScanner = 'find'
let g:NERDSpaceDelims = 1
" python does some stupid shit where the defualt commenter already includes
" the space nicely
let g:NERDCustomDelimiters = {'python': {'left': '#'}}
" allow fancy python highlighting
let g:python_highlight_all = 1
" let g:indentLine_setColors = 0
" let g:blamer_delay = 250
" let g:blamer_prefix = ' >>> '
let g:vim_json_syntax_conceal = 0
let g:git_messenger_extra_blame_args = "-w"
"let g:tagbar_file_size_limit = 200000
"if executable('ag')
  "" Use ag over grep
  "set grepprg="ag --vimgrep"
  "let g:ackprg = 'ag --vimgrep'
"endif

" vim-session options
" let g:session_autosave = 'no'
" let g:session_autoload = 'no'
" set sessionoptions+=tabpages,globals

nnoremap <leader>tf <cmd>Telescope find_files<CR>
nnoremap <leader>tg <cmd>Telescope git_files<CR>
nnoremap <leader>tb <cmd>Telescope buffers<CR>
nnoremap <leader>tl <cmd>Telescope live_grep<CR>
nnoremap <leader>tL <cmd>Telescope live_grep grep_open_files=true<CR>
nnoremap <leader>ts <cmd>Telescope grep_string<CR>
nnoremap <leader>tS <cmd>Telescope grep_string grep_open_files=true<CR>
nnoremap <leader>tq <cmd>Telescope quickfix<CR>
nnoremap <leader>tQ <cmd>Telescope quickfixhistory<CR>

" function! GF(...)
  " call fzf#run({'dir': a:1, 'source': 'find . -type f', 'options':['-1', '--query', expand('<cfile>')], 'sink': 'e'})
  " call fzf#vim#files('.', fzf#vim#with_preview({'options': ['--query', expand('<cword>')]}))<cr>
" endfunction

" function! NearestMethodOrFunction() abort
"   return get(b:, 'vista_nearest_method_or_function', '??')
" endfunction

let g:gutentags_define_advanced_commands=1
let g:gutentags_exclude_filetypes=['c', 'cpp', 'objc', 'objcpp', 'swift']

" command! -nargs=* GF :call GF(<f-args>)
" nnoremap gf :GF .<CR>
" nnoremap <leader>gb :BlamerToggle <CR>
" nnoremap <leader>it :IndentLinesToggle <CR>
nnoremap <leader>he :EnableHL <CR>
nnoremap <leader>hd :DisableHL <CR>
nnoremap <leader>sp :set invpaste <CR>
" nnoremap <leader>it :IBLToggle <CR>
" nnoremap <leader>vt :Vista!! <CR>

" #autocmd VimEnter * call vista#RunForNearestMethodOrFunction()
" autocmd FileType python set omnifunc=python3complete#Complete

" let g:SuperTabDefaultCompletionType = "context"
" autocmd FileType *
  " \ if &omnifunc != '' |
  " \   call SuperTabChain(&omnifunc, "<c-p>") |
  " \ endif

set rtp+=/opt/homebrew/opt/fzf
 command! -nargs=+ Bvimgrep call GrepBuffers(<q-args>)
