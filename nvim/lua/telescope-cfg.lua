require('telescope').setup{
  defaults = {
    vimgrep_arguments = {
        "ag",
        "--nocolor",
        "--noheading",
        "--numbers",
        "--column",
        "--smart-case",
        "--silent",
        "--vimgrep",
    },
    layout_strategy = 'flex',
    layout_config = {
      flex = { flip_columns = 200 },
    },
    path_display={"smart"},
  },
  extensions = {
    ["ui-select"] = {
      require("telescope.themes").get_dropdown {
        -- how to only use this strategy for codeactions?
        -- layout_strategy = 'cursor',
        --previewer = false,
      },
      specific_opts = {
        -- nice and all, but the vim.lsp.buf.code_actions() fn doesn't pass opts.kind = "codeaction", so it doesn't have a way to take effect
        ["codeaction"] = {
          layout_strategy = 'cursor',
        },
      },
    }
  },
}
require('telescope').load_extension('fzf')
require('telescope').load_extension('ui-select')
