local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local function context_status()
  local navic = require('nvim-navic')
  local nvts = require('nvim-treesitter')
  if navic.is_available() then
    return navic.get_location()
  else
    return nvts.statusline()
  end
end

return require('lazy').setup(
{
  {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = function()
      require('lualine').setup({
        options = {
          -- icons_enabled=false,
          component_separators = { left = '|', right = '|'},
          section_separators = { left = '', right = ''},
        },
        sections = {
          lualine_a = {'mode'},
          lualine_b = {
            'filename',
            'branch',
            'diff',
            'diagnostics',
            function()
              return require('lsp-progress').progress()
            end,
          },
          lualine_c = {
            context_status,
          },
          -- lualine_c = {'filename', 'gutentags#statusline'},
          lualine_x = {'encoding', 'filetype'},
          lualine_y = {'progress'},
          lualine_z = {'location'}
        },
        -- winbar = {
        --   lualine_a = {},
        --   lualine_b = {'gutentags#statusline'},
        --   lualine_c = {},
        --   lualine_x = {},
        --   lualine_y = {'buffers'},
        --   lualine_z = {},
        -- },
      })
    end,
  },
  -- 'scrooloose/nerdcommenter',
  -- supertab/completion
  {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end,
  },
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'quangnguyen30192/cmp-nvim-tags',
      'saadparwaiz1/cmp_luasnip',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-cmdline',
      'onsails/lspkind.nvim',
    },
  },
  {
    "L3MON4D3/LuaSnip",
    -- follow latest release.
    version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
    -- install jsregexp (optional!).
    build = "make install_jsregexp",
    dependencies = { "rafamadriz/friendly-snippets" },
  },
  --- seems like this got moved to here, but redirects
  -- 'ervandew/supertab', -- find neovim replacement?
  -- 'vim-scripts/OmniCppComplete',
  ---'nachumk/systemverilog.vim',
  -- 'bfrg/vim-cpp-modern',
  'keith/swift.vim', -- just syntax
  -- 'Yggdroot/indentLine', -- finding replacement
  {
    "shellRaining/hlchunk.nvim",
    event = { "UIEnter" },
    config = function()
      require("hlchunk").setup(
        {
          indent = {
            enable = true,
            chars = { "│", "¦", "┆", "┊", },
          },
          line_num = {
            use_treesitter=true,
            enable=true,
          },
          chunk = {
            max_file_size = 4 * 1024 * 1024,
            enable=true,
            -- use_treesitter = false,
            duration = 100,
            delay = 250,
          },
          blank = {
            enable=true,
          },
        }
      )
    end,
  },
  -- {
    -- "lukas-reineke/indent-blankline.nvim",
    -- main = "ibl",
    -- opts = {
      -- indent = {
        -- char =  { "│", "¦", "┆", "┊",  },
      -- },
      -- scope = {
        -- show_exact_scope = true,
      -- },
    -- },
  -- },
  -- {
    -- "HiPhish/rainbow-delimiters.nvim",
    -- config = function ()
      -- require 'rainbow-delimiters.setup'.setup ({
        -- strategy = {
          -- [''] = rainbow_delimiters.strategy['global'], -- ?? doesn't seem to work, scope issue on rainbow_delimiters
        -- },
      -- })
    -- end,
  -- },
  'elzr/vim-json',
  ---'vim-python/python-syntax',
  ---'tpope/vim-surround',
  -- 'gcmt/taboo.vim', -- double check if still needed or neovim native replacement
  -- {
    -- 'xolox/vim-misc',
    -- dependencies = { 'xolox/vim-session' },
  -- },
  ---'preservim/tagbar',
  ---Git Integrations
  'rhysd/git-messenger.vim',
  { 'FabijanZulj/blame.nvim' },
  -- {
    -- 'junegunn/fzf',
    -- dependencies = { 'junegunn/fzf.vim' },
  -- },
  'ludovicchabant/vim-gutentags',
  --'liuchengxu/vista.vim',

  {
    'nvim-treesitter/nvim-treesitter',
    dependencies = { 'nvim-treesitter/nvim-treesitter-context' },
  },
  'chentoast/marks.nvim',
  {
  'nvim-telescope/telescope.nvim', branch = '0.1.x',
    dependencies = { 'nvim-lua/plenary.nvim', 'nvim-telescope/telescope-ui-select.nvim' },
  },
  { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
  'neovim/nvim-lspconfig',
  {
    'SmiteshP/nvim-navic',
    config = function ()
      local lspconfig = require('lspconfig')
      require('nvim-navic').setup({
        lsp = {
          auto_attach = true,
        },
        highlight = false,
        safe_output = true,
      })
    end
  },
  -- {
    -- 'utilyre/barbecue.nvim',
    -- dependencies = { 'SmiteshP/nvim-navic' },
    -- config = function ()
      -- require('barbecue').setup()
    -- end,
  -- },
  -- 'j-hui/fidget.nvim',
  { 'linrongbin16/lsp-progress.nvim',
    config = function()
      require('lsp-progress').setup(
      {
        client_format = function(client_name, spinner, series_messages)
          if #series_messages == 0 then
            return nil
          end
          return {
            name = client_name,
            body = spinner .. " " .. table.concat(series_messages, ", "),
          }
        end,
        format = function(client_messages)
          --- @param name string
          --- @param msg string?
          --- @return string
          local function stringify(name, msg)
            return msg and string.format("%s %s", name, msg) or name
          end

          -- local sign = "*" -- was nf-fa-gear \uf013
          local sign = ""
          local lsp_clients = vim.lsp.get_active_clients()
          local messages_map = {}
          for _, climsg in ipairs(client_messages) do
            messages_map[climsg.name] = climsg.body
          end

          if #lsp_clients > 0 then
            table.sort(lsp_clients, function(a, b)
              return a.name < b.name
            end)
            local builder = {}
            for _, cli in ipairs(lsp_clients) do
              if
                type(cli) == "table"
                and type(cli.name) == "string"
                and string.len(cli.name) > 0
              then
                if messages_map[cli.name] then
                  table.insert(
                    builder,
                    stringify(cli.name, messages_map[cli.name])
                  )
                else
                  table.insert(builder, stringify(cli.name))
                end
              end
            end
            if #builder > 0 then
              return sign .. " " .. table.concat(builder, ", ")
            end
          end
          return ""
        end,
      })
    end
  },
  {
    'kevinhwang91/nvim-ufo',
    dependencies = { 'kevinhwang91/promise-async' },
  },
  { 'rhysd/vim-llvm' },
  {
    'MeanderingProgrammer/render-markdown.nvim',
     dependencies = { 'nvim-treesitter/nvim-treesitter', 'nvim-tree/nvim-web-devicons' }, -- if you prefer nvim-web-devicons
    ---@module 'render-markdown'
    ---@type render.md.UserConfig
     opts = {},
  },
  -- 'p00f/clangd_extensions.nvim',
  -- {
    -- "wojciech-kulik/xcodebuild.nvim",
    -- dependencies = {
      -- "nvim-telescope/telescope.nvim",
      -- "MunifTanjim/nui.nvim",
    -- },
    -- config = function()
    -- require("xcodebuild").setup({
        -- -- put some options here or leave it empty to use default settings
      -- auto_save = false,
    -- })
    -- vim.keymap.set("n", "<leader>X", "<cmd>XcodebuildPicker<cr>", { desc = "Show Xcodebuild Actions" })
    -- vim.keymap.set("n", "<leader>xf", "<cmd>XcodebuildProjectManager<cr>", { desc = "Show Project Manager Actions" })
    -- vim.keymap.set("n", "<leader>xb", "<cmd>XcodebuildBuild<cr>", { desc = "Build Project" })
    -- end,
  -- },
  -- colors
  'Shatur/neovim-ayu',
  {
    "scottmckendry/cyberdream.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      require("cyberdream").setup({
        -- Recommended - see "Configuring" below for more config options
        transparent = true,
        italic_comments = true,
        hide_fillchars = true,
        borderless_telescope = true,
        terminal_colors = true,
      })
    end,
  },
  {
    'uloco/bluloco.nvim',
    lazy = false,
    priority = 1000,
    dependencies = { 'rktjmp/lush.nvim' },
    config = function()
      require("bluloco").setup({
        style = "dark",               -- "auto" | "dark" | "light"
        transparent = false,
        italics = false,
        terminal = vim.fn.has("gui_running") == 1, -- bluoco colors are enabled in gui terminals per default.
        guicursor   = true,
      })
    end,
  },
  {
    'rebelot/kanagawa.nvim',
    lazy = false,
    priority = 1000,
    config = function()
      require ('kanagawa').setup({
        theme = "dragon",
      })
    end,
  },
  {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
    lazy = false,
  },
  {
    'AlexvZyl/nordic.nvim',
    lazy = false,
    priority = 1000,
    config = function()
      require 'nordic' .setup({
        transparent = false,
        swap_backgrounds = true,
      })
    end,
  },
  {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
  },
  {
    "AhmedAbdulrahman/vim-aylin",
    lazy = false,
    priority = 1000,
  },
  {
    "olimorris/onedarkpro.nvim",
    priority = 1000, -- Ensure it loads first
  },
},
{
  -- {
    -- ui = {
      -- icons = {
        -- cmd = "⌘",
        -- config = "🛠",
        -- event = "📅",
        -- ft = "📂",
        -- import = "🛬",
        -- init = "⚙",
        -- keys = "🗝",
        -- plugin = "🔌",
        -- runtime = "💻",
        -- source = "📄",
        -- start = "🚀",
        -- task = "📌",
        -- lazy = "💤 ",
        -- loaded = "●",
        -- not_loaded = "○",
        -- list = {
          -- "●",
          -- "➜",
          -- "★",
          -- "‒",
        -- },
      -- },
    -- },
  -- }
})
