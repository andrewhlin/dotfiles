local lspconfig = require 'lspconfig'
-- Set up lspconfig.
local util = lspconfig.util
local capabilities = require('cmp_nvim_lsp').default_capabilities()

local telescope = require('telescope.builtin')
local telpickers = require('telescope.pickers')
local telfinders = require('telescope.finders')
local telconf = require('telescope.config').values
local tel_make_entry = require('telescope.make_entry')
local async = require('plenary.async')

local handlers = {
  ["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {border = 'rounded'}),
  -- ["txtdocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, borders),
}

local function transform_objc(_, filetype)
  if filetype == 'objcpp' then
    return 'objective-cpp'
  elseif filetype == 'objc' then
    return 'objective-c'
  end
  return filetype
end

local function toggle_inlay()
  vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled({}), nil)
end

local function populate_type_locations(result, _)
  local locations = {}
  for _, th_type in pairs(result) do
    local range = th_type["range"]
    table.insert(locations, {
      filename = vim.uri_to_fname(th_type.uri),
      text = th_type.name,
      lnum = range.start.line + 1,
      col = range.start.character + 1,
    })
  end
  return locations
end

local function populate_call_locations(result, direction)
  local locations = {}
  for _, ch_call in pairs(result) do
    local ch_item = ch_call[direction]
    for _, rng in pairs(ch_call.fromRanges) do
      table.insert(locations, {
        filename = vim.uri_to_fname(ch_item.uri),
        text = ch_item.name,
        lnum = rng.start.line + 1,
        col = rng.start.character + 1,
      })
    end
  end
  return locations
end

local dispatch = {
  ["type"] = populate_type_locations,
  ["call"] = populate_call_locations,
}

local function show_hierarchy(opts, request, method, title, direction, item)
  vim.lsp.buf_request(opts.bufnr, method, { item = item }, function(err, result)
    if err then
      vim.api.nvim_err_writeln("Error handling " .. title .. ": " .. err.message)
      return
    end

    if not result or vim.tbl_isempty(result) then
      return
    end

    local locations = dispatch[request](result, direction)

    telpickers
      .new(opts, {
        prompt_title = title,
        finder = telfinders.new_table {
          results = locations,
          entry_maker = opts.entry_maker or tel_make_entry.gen_from_quickfix(opts),
        },
        previewer = telconf.qflist_previewer(opts),
        sorter = telconf.generic_sorter(opts),
        push_cursor_on_edit = true,
        push_tagstack_on_edit = true,
      })
      :find()
  end)
end

local function hierarchy_select(hierarchy_items)
  -- vim.print(hierarchy_items)
  if not hierarchy_items or #hierarchy_items == 0 then
    -- print("No results.")
    return
  end
  if #hierarchy_items == 1 then
    return hierarchy_items[1]
  end
  local items = {}
  for i, item in pairs(hierarchy_items) do
    local entry = item.detail or item.name
    table.insert(items, string.format("%d. %s", i, entry))
  end
  local select = async.wrap(function(options, callback)
    vim.ui.select(options, {}, callback)
    end, 2)
  local choice = select(items, function (_, idx)
    return idx
  end)
  if choice == nil then
    return
  else
    return hierarchy_items[choice]
  end
end

local req_map = {
  ["type"] = {
    request = "textDocument/prepareTypeHierarchy",
    subrequests = {
      ["super"] = { method = "typeHierarchy/supertypes", title = "LSP Supertypes" },
      ["sub"] = { method = "typeHierarchy/subtypes", title = "LSP Subtypes" },
    }
  },
  ["call"] = {
    request = "textDocument/prepareCallHierarchy",
    subrequests = {
      ["to"] = { method = "callHierarchy/outgoingCalls", title = "LSP Outgoing Calls" },
      ["from"] = { method = "callHierarchy/incomingCalls", title = "LSP Incoming Calls" },
    }
  }
}

local function hierarchy(opts, request, direction)
  local params = vim.lsp.util.make_position_params()
  local details = req_map[request]
  if not details then
    return
  end
  local subrequest = details.subrequests[direction]
  if not subrequest then
    return
  end
  vim.lsp.buf_request(opts.bufnr, details.request, params, function(err, result)
    if err then
      vim.api.nvim_err_writeln("Error when preparing " .. request .. " hierarchy: " .. err)
      return
    end

    local hierarchy_item = hierarchy_select(result)
    if not hierarchy_item then
      return
    end

    show_hierarchy(opts, request, subrequest.method, subrequest.title, direction, hierarchy_item)
  end)
end

--TODO: if/when neovim adds dynamic capability registration for textDocument/typeDefinition, need to wrap this with the check
local function supertypes(opts)
  opts = opts or {}
  hierarchy(opts, "type", "super")
end

local function subtypes(opts)
  opts = opts or {}
  hierarchy(opts, "type", "sub")
end

local function outgoing_calls(opts)
  opts = opts or {}
  hierarchy(opts, "call", "to")
end

local function incoming_calls(opts)
  opts = opts or {}
  hierarchy(opts, "call", "from")
end

capabilities.textDocument.foldingRange = {
    dynamicRegistration = false,
    lineFoldingOnly = true
}

local function sourcekit_root(filename, bufnr)
  local ft = vim.api.nvim_get_option_value('filetype', {buf=bufnr})
  local xcode_match = util.root_pattern('buildServer.json')(filename)
    or util.root_pattern('*.xcodeproj', '*.xcworkspace')(filename)
        -- better to keep it at the end, because some modularized apps contain multiple Package.swift files
  local db_match = util.root_pattern('compile_commands.json', 'Package.swift')(filename)
  local git_match = util.find_git_ancestor(filename)
  if (ft == 'cpp' or ft == 'c') then
    return xcode_match
  end
  return xcode_match or db_match or git_match
end

lspconfig.sourcekit.setup({
  -- not actually needed, the default is better
  -- root_dir = lspconfig.util.root_pattern('.git', 'Package.swift', 'compile_commands.json'),
  -- root_dir = sourcekit_root,
  -- cmd = {'sourcekit-lsp', '--compilation-db-search-path', '<extra/path/from/root/to/compile_commands.json>'},
  capabilities = vim.tbl_deep_extend('keep', capabilities, {
    workspace = {
      didChangeWatchedFiles = {
        dynamicRegistration = true,
      },
    },
  }),
  handlers = handlers,
  -- filetypes = {'swift', 'objc', 'objcpp', 'c', 'cpp'},
  filetypes = {'swift'},
  get_language_id = transform_objc,
  -- single_file_support = true,
})

local function clangd_root(filename, _)
  local xcode_match = util.root_pattern('buildServer.json')(filename)
    or util.root_pattern('*.xcodeproj', '*.xcworkspace')(filename)
  if (xcode_match ~= nil) then
    return nil
  end
  local clang_match = util.root_pattern('.clangd', '.clang-tidy', '.clang-format', 'compile_commands.json', 'compile_flags.txt', 'configure.ac')(filename)
  local git_match = util.find_git_ancestor(filename)
  return clang_match or git_match
end


lspconfig.clangd.setup({
  -- not actually needed, the default is better
  -- root_dir = lspconfig.util.root_pattern('.git', 'compile_commands.json'),
  -- root_dir = clangd_root,
  -- cmd = {'clangd', '--compile-commands-path', '<extra/path/from/root/to/compile_commands.json>'},
  init_options = {
    clangdFileStatus = true
  },
  capabilities = capabilities,
  handlers = handlers,
  filetypes = {'c', 'cpp', 'objc', 'objcpp'},
  get_language_id = transform_objc,
})

lspconfig.pyright.setup{}

lspconfig.lua_ls.setup {
  on_init = function(client)
    local path = client.workspace_folders[1].name
    if vim.loop.fs_stat(path..'/.luarc.json') or vim.loop.fs_stat(path..'/.luarc.jsonc') then
      return
    end

    client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
      runtime = {
        -- Tell the language server which version of Lua you're using
        -- (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT'
      },
      -- Make the server aware of Neovim runtime files
      workspace = {
        checkThirdParty = false,
        library = {
          vim.env.VIMRUNTIME
          -- Depending on the usage, you might want to add additional paths here.
          -- "${3rd}/luv/library"
          -- "${3rd}/busted/library",
        }
        -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
        -- library = vim.api.nvim_get_runtime_file("", true)
      }
    })
  end,
  settings = {
    Lua = {}
  }
}

lspconfig.biome.setup{}

vim.diagnostic.config({
  float = {
    border = 'rounded',
    show_header = true,
  },
})

vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  desc = 'LSP Actions',
  callback = function(args)
    -- buffer local mappings
    local opts = { buffer = args.buf }
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    if client == nil then return end
    -- all of these are basically table stakes
    -- go to definition
    vim.keymap.set('n','gd', telescope.lsp_definitions, opts)
    -- find references of a type
    vim.keymap.set('n','gr', telescope.lsp_references, opts)
    -- go to implementation
    vim.keymap.set('n','gi', telescope.lsp_implementations, opts)
    -- go to declaration
    vim.keymap.set('n','gD', vim.lsp.buf.declaration, opts)
    -- rename
    vim.keymap.set('n','gn', vim.lsp.buf.rename, opts)
    --puts doc header info into a float page
    vim.keymap.set('n','K', vim.lsp.buf.hover, opts)

    vim.keymap.set('n','ls', vim.lsp.buf.signature_help, opts)

    vim.keymap.set('n','[g', vim.diagnostic.goto_prev, opts)
    vim.keymap.set('n','g]', vim.diagnostic.goto_next, opts)

    vim.keymap.set({'n','v'},'ca', vim.lsp.buf.code_action, opts)

    vim.keymap.set('n', '<leader>tW', telescope.lsp_dynamic_workspace_symbols,opts)
    vim.keymap.set('n', '<leader>tr', telescope.lsp_references,opts)
    vim.keymap.set('n', '<leader>td', telescope.lsp_document_symbols,opts)
    vim.keymap.set('n', '<leader>tw', telescope.lsp_workspace_symbols,opts)
    vim.keymap.set('n', '<leader>tD', telescope.diagnostics,opts)
    vim.keymap.set('n', '<leader>gD', vim.diagnostic.open_float,opts)
    -- workspace management. Necessary for multi-module projects
    -- vim.keymap.set('n','<space>wa',vim.lsp.buf.add_workspace_folder, opts)
    -- vim.keymap.set('n','<space>wr',vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n','<space>wl',function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end,opts)

    if client.supports_method('textDocument/typeDefinition') then
      -- type definition
      vim.keymap.set('n','gt', telescope.lsp_type_definitions, opts)
    end
    --
    if client.supports_method('textDocument/documentHighlight') then
      vim.api.nvim_create_autocmd("CursorHold",  { buffer = args.buf, callback = function() vim.lsp.buf.document_highlight() end })
      vim.api.nvim_create_autocmd("CursorHoldI", { buffer = args.buf, callback = function() vim.lsp.buf.document_highlight() end })
      vim.api.nvim_create_autocmd("CursorMoved", { buffer = args.buf, callback = function() vim.lsp.buf.clear_references() end })
    end

    if client.supports_method('textDocument/inlayHint') then
      vim.keymap.set('n', '<leader>ti', toggle_inlay, opts)
    end

    if client.supports_method('textDocument/typeHierarchy') then
      vim.keymap.set('n', 'gh', supertypes, opts)
      vim.keymap.set('n', 'gH', subtypes, opts)
    end

    if client.supports_method('callHierarchy/incomingCalls') then
      vim.keymap.set('n', 'gC', incoming_calls, opts)
    end

    if client.supports_method('callHierarchy/outgoingCalls') then
      vim.keymap.set('n', 'gO', outgoing_calls, opts)
    end

    -- not currently supported by clangd or sourcekit-lsp
    if client.supports_method('textDocument/codeLens') then
      vim.api.nvim_create_autocmd({"BufEnter", "CursorHold", "InsertLeave"}, {buffer = args.buf, callback = function() vim.lsp.codelens.refresh({args.buf}) end })
    end

    --TODO: look into folding
    --TODO: look into onTypeFormatting
    --TODO: telescope previewer for code_actions (lspsaga.nvim or actions-preview.nvim?)
    --TODO: Write telescope picker for typeHierarchy superkind/subkinds? With nesting?

  end,
})
