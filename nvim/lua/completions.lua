local cmp = require'cmp'

local check_backspace = function()
  local col = vim.fn.col "." - 1
  return col == 0 or vim.fn.getline("."):sub(col, col):match "%s"
end

local luasnip = require("luasnip")

require("luasnip.loaders.from_vscode").lazy_load()

cmp.setup(
{
  snippet = {
    -- REQUIRED - you must specify a snippet engine
    expand = function(args)
      -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
      require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
      -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
      -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
      -- vim.snippet.expand(args.body) -- For native neovim snippets (Neovim v0.10+)
    end,
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
    -- documentation = {
      -- border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
    -- },
  },
  mapping = cmp.mapping.preset.insert({
    ["<C-k>"] = cmp.mapping.select_prev_item(),
		["<C-j>"] = cmp.mapping.select_next_item(),
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    -- Accept currently selected item. If none selected, `select` first item.
    -- Set `select` to `false` to only confirm explicitly selected items.
    ["<CR>"] = cmp.mapping.confirm { select = true },
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expandable() then
        luasnip.expand()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif check_backspace() then
        fallback()
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
  }),
  sources = cmp.config.sources(
  {
    { name = 'nvim_lsp', group_index = 1 },
    { name = 'luasnip', group_index = 1 }, -- For luasnip users.
    -- { name = 'buffer' },
    { name = 'tags', group_index = 2 },
  }),
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      -- Kind icons
      -- vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
      -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
      vim_item.menu = ({
        nvim_lsp = "[LSP]",
        luasnip = "[Snippet]",
        -- buffer = "[Buffer]",
        tags = "[Tags]",
        -- path = "[Path]",
      })[entry.source.name]
      -- return vim_item
      return require('lspkind').cmp_format({ with_text = false }) (entry, vim_item)
    end,
  },
  confirm_opts = {
    behavior = cmp.ConfirmBehavior.Replace,
    select = false,
  },
  experimental = {
    ghost_text = true,
  },
})

-- Set configuration for specific filetype.
cmp.setup.filetype('tmux', {
  sources = cmp.config.sources({
    { name = 'buffer' },
  })
})
--
-- Set configuration for specific filetype.
-- cmp.setup.filetype('gitcommit', {
  -- sources = cmp.config.sources({
    -- { name = 'git' }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
  -- }, {
    -- { name = 'buffer' },
  -- })
-- })

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ '/', '?' }, {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'nvim_lsp', group_index = 1 },
    { name = 'tags', group_index = 2 },
    { name = 'buffer', group_index = 3 },
  },
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      -- Kind icons
      -- vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
      -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
      vim_item.menu = ({
        nvim_lsp = "[LSP]",
        tags = "[Tags]",
        buffer = "[Buffer]",
        -- path = "[Path]",
      })[entry.source.name]
      return vim_item
    end,
  },
  experimental = {
    ghost_text = true,
  },
})

-- `:` cmdline setup.
-- Use cmp.cmdline only for '/' search completion because it does not play nice
-- with:
-- - $ENV_VARS
-- - wildcard expansion (%:h:p, ...)
-- - */** notation
-- Skipping the setup entirely prevents cmdline completion from working after
-- searching once (insertion of '^I' when pressing Tab).
local function send_wildchar()
    local char = vim.fn.nr2char(vim.opt.wildchar:get())
    local key = vim.api.nvim_replace_termcodes(char, true, false, true)
    vim.api.nvim_feedkeys(key, "nt", true)
end

cmp.setup.cmdline(":", {
    mapping = {
        ["<Tab>"] = {c = send_wildchar}
    },
    sources = cmp.config.sources({})
})

--Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline(':', {
  -- mapping = cmp.mapping.preset.cmdline(),
  -- sources = cmp.config.sources({
    -- { name = 'path' }
  -- }, {
    -- { name = 'cmdline' }
  -- }),
  -- matching = { disallow_symbol_nonprefix_matching = false }
-- })

-- Set up lspconfig.
-- local capabilities = require('cmp_nvim_lsp').default_capabilities()
-- -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
-- require('lspconfig')['<YOUR_LSP_SERVER>'].setup {
  -- capabilities = capabilities
-- }
