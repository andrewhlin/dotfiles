require ('catppuccin').setup ({
  flavour = 'mocha',
  color_overrides = {
    mocha = {
      base = "#000000",
      mantle = "#000000",
      crust = "#000000",
    },
  },
  integrations = {
    telescope = {
      enabled = true,
      style = "nvchad",
    },
    treesitter = true,
  },
})
