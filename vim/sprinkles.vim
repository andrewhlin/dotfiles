" ============================================================================
" File: onedark.vim
" Description: onedark colorscheme for Lightline (itchyny/lightline.vim)
" Author: novadev94 <novadev94@gmail.com>
" Source: https://github.com/novadev94/lightline-onedark
" Last Modified: 06 Oct 2016
" ===========================================================================

"let s:bold = get(g:, 'lightline#sprinkles#disable_bold_style', 0) ? '' : 'bold'

" Colour codes that are used in the original onedark.vim theme
let s:light_red     = [ '#ff5252', 204 ]
let s:dark_red      = [ '#be5046', 196 ]
let s:green         = [ '#d4e157', 114 ]
let s:blue          = [ '#42a5f5', 39 ]
let s:cyan          = [ '#00e5ff', 38 ]
let s:magenta       = [ '#c678dd', 170 ]
let s:light_yellow  = [ '#e5c07b', 180 ]
let s:dark_yellow   = [ '#d19a66', 173 ]
let s:orange        = [ '#ffcb6b', 214]

let s:black         = [ '#202020', 235 ]
let s:white         = [ '#ffffff', 145 ]
let s:comment_grey  = [ '#5c6370', 59 ]
let s:gutter_grey   = [ '#636d83', 238 ]
let s:cursor_grey   = [ '#282a2e', 236 ]
let s:special_grey  = [ '#373b41', 238 ]
let s:visual_grey   = s:special_grey
let s:vertsplit     = [ '#181a1f', 59 ]

let s:menu_grey     = s:visual_grey
let s:tab_color     = s:blue
let s:normal_color  = s:blue
let s:insert_color  = s:green
let s:replace_color = s:light_red
let s:visual_color  = s:orange
let s:active_bg     = s:visual_grey
let s:inactive_bg   = s:special_grey

let s:p = {'normal': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'inactive': {}, 'tabline': {}}

let s:p.normal.left     = [ [ s:black, s:normal_color ], [ s:white, s:active_bg ] ]
let s:p.normal.right    = [ [ s:black, s:normal_color ], [ s:white, s:active_bg ] ]
let s:p.normal.middle   = [ [ s:normal_color, s:cursor_grey ] ]

let s:p.insert.left     = [ [ s:black, s:insert_color ], [ s:white, s:active_bg ] ]
let s:p.insert.right    = [ [ s:black, s:insert_color ], [ s:white, s:active_bg ] ]
let s:p.insert.middle   = [ [ s:insert_color, s:cursor_grey ] ]

let s:p.replace.left    = [ [ s:black, s:replace_color ], [ s:white, s:active_bg ] ]
let s:p.replace.right   = [ [ s:black, s:replace_color ], [ s:white, s:active_bg ] ]
let s:p.replace.middle  = [ [ s:replace_color, s:cursor_grey ] ]

let s:p.visual.left     = [ [ s:black, s:visual_color ], [ s:white, s:active_bg ] ]
let s:p.visual.right    = [ [ s:black, s:visual_color ], [ s:white, s:active_bg ] ]
let s:p.visual.middle   = [ [ s:visual_color, s:cursor_grey ] ]

let s:p.inactive.left   = [ [ s:white, s:inactive_bg ], [ s:white, s:inactive_bg ] ]
let s:p.inactive.right  = [ [ s:white, s:inactive_bg ], [ s:white, s:inactive_bg ] ]
let s:p.inactive.middle = [ [ s:white, s:inactive_bg ] ]

let s:p.tabline.left    = [ [ s:gutter_grey, s:cursor_grey ] ]
let s:p.tabline.right   = [ [ s:tab_color, s:cursor_grey ] ]
let s:p.tabline.middle  = [ [ s:black, s:black ] ]
let s:p.tabline.tabsel  = [ [ s:black, s:blue ] ]
let s:p.tabline.bufsel  = [ [ s:tab_color, s:visual_grey ] ]
let s:p.tabline.tabsep  = [ [ s:white, s:cursor_grey ] ]

let s:p.normal.error    = [ [ s:black, s:light_red ] ]
let s:p.normal.warning  = [ [ s:black, s:orange ] ]

let g:lightline#colorscheme#sprinkles#palette = lightline#colorscheme#flatten(s:p)
